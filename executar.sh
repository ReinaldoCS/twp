#!/bin/ksh

parm1=$1
parm2=$2

cycle="${parm1:0:2}"
mes="${parm1:2:2}"
ano="${parm1:4:4}"

echo "cycle: ${cycle}"
sleep 10
echo "mes: ${mes}"
echo "ano: ${ano}"
# echo "Cyclo" ${teste:0:2}
# echo "mes" ${teste:2:2}
# echo "ano" ${teste:4:4}


#exit 0

if [ "$#" = "1" ]; then
    if [ "$1" = "-help" ] || [ "$1" = "-h" ] || [ "$1" = "h" ] || [ "$1" = "help" ]; then
        echo ""
        echo "Uso: ./Scheduler_Billing_CI.sh <param1> <param2> <param3> <param4> <param5>"
        echo ""
        echo "exibir informações de ajuda"
        echo ""
        echo "Informações do paramentros:"
        echo ""
        echo "  onde <param1> é o valor do ciclo com dois numero <CC>"
        echo "  onde <param2> é o valor do mês com dois numero <MM>"
        echo "  onde <param3> é o valor do ano com quatro numero <YYYY>"
        echo "  onde <param4> é a sequencia de jobs (ex valor: 1-13) ou um job específico (ex valor: 2)"
        echo "  onde <param5> é o opcional:" 
        echo "      passe apenas o valor de y caso deseja que os operacionais" 
        echo "      seja executados. Se for a primeira execução se um novo ciclo,"
        echo "      mes e ano recomendo passaro valor de y, caso ainda esteja"
        echo "      testando o billing do mesmo ciclo já faturado e tenha"
        echo "      feito o UNDO recomendo não passar nenhum valor para o param5"
        echo ""
        echo ""
        echo "Informações de comandos:"
        echo ""
        echo "-help, -h, help, h        exibir informações de ajuda"
        echo "-joblist, joblist         exibe lista de jobs"
        exit
    fi

    if [ "$1" = "-joblist" ] || [ "$1" = "joblist" ]; then
        echo "exibir lista de jobs"
    fi
fi

./mediaDoAluno.sh $parm1 $parm2
./script2.sh $parm1 $parm2
